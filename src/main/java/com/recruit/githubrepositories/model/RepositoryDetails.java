package com.recruit.githubrepositories.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.time.ZonedDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RepositoryDetails {

    private String fullName;
    private String description;
    private String cloneUrl;
    private long stars;
    private ZonedDateTime createdAt;

    @JsonGetter("fullName")
    public String getFullName() {
        return fullName;
    }

    @JsonSetter("full_name")
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @JsonGetter("description")
    public String getDescription() {
        return description;
    }

    @JsonSetter("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonGetter("cloneUrl")
    public String getCloneUrl() {
        return cloneUrl;
    }

    @JsonSetter("clone_url")
    public void setCloneUrl(String cloneUrl) {
        this.cloneUrl = cloneUrl;
    }

    @JsonGetter("stars")
    public long getStars() {
        return stars;
    }

    @JsonSetter("stargazers_count")
    public void setStars(long stars) {
        this.stars = stars;
    }

    @JsonGetter("createdAt")
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    @JsonSetter("created_at")
    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
