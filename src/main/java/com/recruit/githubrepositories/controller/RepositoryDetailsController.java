package com.recruit.githubrepositories.controller;

import com.recruit.githubrepositories.model.RepositoryDetails;
import com.recruit.githubrepositories.service.RepositoryDetailsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RepositoryDetailsController {

    private RepositoryDetailsService repositoryDetailsService;

    public RepositoryDetailsController(RepositoryDetailsService repositoryDetailsService) {
        this.repositoryDetailsService = repositoryDetailsService;
    }

    @GetMapping("/repositories/{owner}/{repository-name}")
    public ResponseEntity<RepositoryDetails> getRepositoryDetails(@PathVariable String owner,
                                                                  @PathVariable("repository-name") String repositoryName) {
            return ResponseEntity.ok(repositoryDetailsService.getRepositoryDetails(owner, repositoryName));
    }
}
