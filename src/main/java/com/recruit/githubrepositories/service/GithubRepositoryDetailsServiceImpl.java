package com.recruit.githubrepositories.service;

import com.recruit.githubrepositories.model.RepositoryDetails;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GithubRepositoryDetailsServiceImpl implements RepositoryDetailsService {

    private RestTemplate restTemplate;
    @Value("${repository.details.url.github}")
    private String githubRepositoryDetailsUrl;

    public GithubRepositoryDetailsServiceImpl(RestTemplateBuilder builder) {
        this.restTemplate = builder.errorHandler(new RestTemplateErrorHandler()).build();
    }

    @Override
    public RepositoryDetails getRepositoryDetails(String owner, String repositoryName) {
        return this.restTemplate
                .getForObject(githubRepositoryDetailsUrl, RepositoryDetails.class, owner, repositoryName);
    }
}
