package com.recruit.githubrepositories.service;

import com.recruit.githubrepositories.model.RepositoryDetails;

public interface RepositoryDetailsService {

    RepositoryDetails getRepositoryDetails(String owner, String repositoryName);
}
